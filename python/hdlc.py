import crcmod

# Constants from ScaSoftware HldcBackendCommon.h
NETIO_FRAME_HDLC_START_OFFSET = 8  # HDLC frame shall be placed starting from this index
HDLC_FRAME_PAYLOAD_OFFSET = 2  # HDLC payload offset in HDLC frame
HDLC_FRAME_TRAILER_SIZE = 2  # HDLC trailer size in HDLC frame

# HDLC I-frame fields
HDLC_CTRL_SEQNR_MASK = 0x07
HDLC_CTRL_NRECVD_SHIFT = 5
HDLC_CTRL_NSEND_SHIFT = 1

# HDLC U-frame control commands
HDLC_CTRL_CONNECT = 0x2F
HDLC_CTRL_RESET = 0x8F
HDLC_CTRL_TEST = 0xE3
HDLC_CTRL_POLLBIT = 0x10


def encode_hdlc_msg_header(addr, seqnr):
    # Control byte: acknowledge up to previous message, by default..(?)
    seqnr &= HDLC_CTRL_SEQNR_MASK
    return bytearray([addr & 0xFF, (seqnr << HDLC_CTRL_NRECVD_SHIFT) | (seqnr << HDLC_CTRL_NSEND_SHIFT)])


def encode_hdlc_ctrl_header(addr, control):
    return bytearray([addr & 0xFF, control & 0xFF])


def encode_hdlc_trailer(data):
    crc16 = crcmod.mkCrcFun(0x11021, rev=True, initCrc=0xFFFF, xorOut=0x0000)
    crc = crc16(data)
    return bytearray([crc & 0xFF, (crc >> 8) & 0xFF])


def verify_hdlc_trailer(data):
    crc16 = crcmod.mkCrcFun(0x11021, rev=True, initCrc=0xFFFF, xorOut=0x0000)
    return crc16(data) == 0
