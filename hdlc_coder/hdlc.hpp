#ifndef FELIX_HDLC_HPP
#define FELIX_HDLC_HPP

#include <cstdint>
#include <cstdlib>

// GBT-SCA stuff
// -------------
#define HDLC_SOF               0x7E
// HDLC I-frame fields
#define HDLC_CTRL_SEQNR_MASK   0x07
#define HDLC_CTRL_NRECVD_SHIFT 5
#define HDLC_CTRL_NSEND_SHIFT  1

// HDLC U-frame control commands
#define HDLC_CTRL_CONNECT      0x2F
#define HDLC_CTRL_RESET        0x8F
#define HDLC_CTRL_TEST         0xE3
#define HDLC_CTRL_POLLBIT      0x10


namespace felix {
namespace hdlc {

enum frame_t {
	UFRAME,
	IFRAME,
	SFRAME
};

/* Encodes an HDLC message header (IFRAME). This implicitly acknowledges the
 receipt of messages up to `seqnr`*/
void encode_hdlc_msg_header(uint8_t* header, int addr, uint64_t seqnr);

/* Encodes an HDLC control message header (UFRAME) */
void encode_hdlc_ctrl_header(uint8_t* header, int addr, int control);

/*
Encodes HDLC trailer. The pointer `data` has to point to a valid HDLC frame
including header and data part. The length field `len` is the length of the
HDLC header plus the length of the message.
*/
void encode_hdlc_trailer(uint8_t* trailer, const uint8_t* data, size_t len);

/* Poor-mans-decoder for HDLC frames. This will cut the HDLC header and trailer.*/
void decode_hdlc_msg(uint8_t** dest_msg, size_t* dest_len, uint8_t* data, size_t len);

/* Returns true if the HDLC message FCS (checksum) is correct. */
bool verify_hdlc_trailer(const uint8_t* data, size_t len);

/* Returns the frame type of an HDLC encoded frame*/
frame_t frame_type(const uint8_t* data);


}
}



#endif
