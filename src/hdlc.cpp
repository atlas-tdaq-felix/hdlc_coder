#include "hdlc_coder/hdlc.hpp"


static const uint8_t NibbleReversed[16] =
  {
   0x0, 0x8, 0x4, 0xc, 0x2, 0xa, 0x6, 0xe,
   0x1, 0x9, 0x5, 0xd, 0x3, 0xb, 0x7, 0xf
  };

// Return 8-bit value 'byte' bit-reversed
static uint8_t
bit_reversed(uint8_t byte)
{
  // Reverse the top and bottom nibble and swap them
  return( (NibbleReversed[byte & 0xF] << 4) | NibbleReversed[byte >> 4] );
}


/*
Calculate CRC-16 value; uses The CCITT-16 Polynomial,
expressed as X^16 + X^12 + X^5 + 1
NB: for GBT-SCA packets need to bit-reverse each byte
    because the bytes are transmitted LSB to MSB (?) (added 28 Oct 2016)
*/
static uint16_t
crc16(int nbytes, const uint8_t *data)
{
  uint16_t crc = (uint16_t) 0xFFFF;
  int index, b;
  for( index=0; index<nbytes; ++index )
    {
      //crc ^= (((uint16_t) data[index]) << 8); // Non bit-reversed
      uint8_t tmp = bit_reversed( data[index] );
      crc ^= (((uint16_t) tmp) << 8);
      for( b=0; b<8; ++b )
        {
          if( crc & (uint16_t) 0x8000 )
            crc = (crc << 1) ^ (uint16_t) 0x1021;
          else
            crc = (crc << 1);
        }
    }
  uint8_t tmp1, tmp2;
  tmp1 = bit_reversed( (uint8_t) ((crc >> 8) & 0xFF) );
  tmp2 = bit_reversed( (uint8_t) (crc & 0xFF) );
  crc = (uint16_t) tmp2 | ((uint16_t) tmp1 << 8);
  return crc;
}


void
felix::hdlc::encode_hdlc_msg_header(uint8_t* header, int addr, uint64_t seqnr)
{
  header[0] = (uint8_t) (addr & 0xFF);
  // Control byte: acknowledge up to previous message, by default..(?)
  seqnr &= 0x7;
  header[1] = (uint8_t) ((seqnr << HDLC_CTRL_NRECVD_SHIFT)|(seqnr << HDLC_CTRL_NSEND_SHIFT));
}


void
felix::hdlc::encode_hdlc_ctrl_header(uint8_t* header, int addr, int control)
{
  header[0] = (uint8_t) (addr & 0xFF);
  header[1] = (uint8_t) (control & 0xFF);
}


void
felix::hdlc::encode_hdlc_trailer(uint8_t* trailer, const uint8_t* data, size_t len)
{
  uint16_t crc = crc16(len, data);
  trailer[0] = (uint8_t) ((crc >> 8) &  0xFF);
  trailer[1] = (uint8_t) (crc & 0xFF);
}


felix::hdlc::frame_t
felix::hdlc::frame_type(const uint8_t* data)
{
  if((data[1] & 0x3) == 0x3) {
    return UFRAME;
  }
  if((data[1] & 0x3) == 0x1) {
    return SFRAME;
  }
  return IFRAME;
}


void
felix::hdlc::decode_hdlc_msg(uint8_t** dest_msg, size_t* dest_len, uint8_t* data, size_t len)
{
  *dest_msg = data + 2;
  *dest_len = len - 4;
}


bool
felix::hdlc::verify_hdlc_trailer(const uint8_t* data, size_t len)
{
  // Verify that the FCS (2-byte trailer) is correct
  return( crc16(len, data) == 0 );
}
