#include <catch2/catch_test_macros.hpp>

#include "hdlc_coder/hdlc.hpp"

#include <cstring>

using namespace felix::hdlc;

TEST_CASE( "encode hello world", "[encode]" )
{
  uint8_t buffer[1024];
  const char* data = "hello world";
  size_t len = std::strlen(data);

  encode_hdlc_msg_header(buffer, 0, 0);
  memcpy(buffer+2, data, len);
  encode_hdlc_trailer(buffer+2+len, buffer, 2+len);

  REQUIRE(frame_type(buffer) == IFRAME);

  uint8_t* recovered_data;
  size_t recovered_len;
  decode_hdlc_msg(&recovered_data, &recovered_len, buffer, len+2+2);

  REQUIRE(recovered_len == len);
  REQUIRE(strncmp((const char*)recovered_data, data, recovered_len) == 0);

  REQUIRE(verify_hdlc_trailer(buffer, len+2+2) == true);
}

TEST_CASE( "crc request", "[crc]" )
{
  const unsigned char data[] = { 0x00, 0x8f, 0x47, 0x8c };  // Reset and CRC
  size_t len = 2;

  REQUIRE(verify_hdlc_trailer(data, len+2) == true);
}

TEST_CASE( "encode reply", "[encode]" )
{
  uint8_t buffer[1024];
  const unsigned char data[] = { 0xFF, 0x63 };
  size_t len = 2;

  memcpy(buffer, data, len);
  encode_hdlc_trailer(buffer+len, buffer, len);

  REQUIRE(verify_hdlc_trailer(buffer, len+2) == true);
}

TEST_CASE( "crc reply", "[crc]" )
{
  const unsigned char data[] = { 0xFF, 0x63, 0xE5, 0x5E };
  size_t len = 2;

  REQUIRE(verify_hdlc_trailer(data, len+2) == true);
}

TEST_CASE( "crc sca request", "[crc]" )
{
  const unsigned char data[] = { 0x00, 0x22, 0x00, 0x14, 0x04, 0xd1, 0x00, 0x00, 0x01, 0x00, 0xfc, 0xd7 };
  size_t len = 10;

  REQUIRE(verify_hdlc_trailer(data, len+2) == true);
}

TEST_CASE( "encode sca header", "[encode]" )
{
  uint8_t buffer[1024];
  const unsigned char data[] = { 0x00, 0x14, 0x04, 0xd1, 0x00, 0x00, 0x01, 0x00, 0xfc, 0xd7 };
  size_t len = 8;

  encode_hdlc_msg_header(buffer, 0, 1);
  memcpy(buffer+2, data, len+2);

  REQUIRE(verify_hdlc_trailer(buffer, len+2+2) == true);
}

TEST_CASE( "encode sca request", "[encode]" )
{
  uint8_t buffer[1024];
  const unsigned char data[] = { 0x00, 0x14, 0x04, 0xd1, 0x00, 0x00, 0x01, 0x00 };
  size_t len = 8;

  encode_hdlc_msg_header(buffer, 0, 1);
  memcpy(buffer+2, data, len);
  encode_hdlc_trailer(buffer+2+len, buffer, 2+len);

  REQUIRE(verify_hdlc_trailer(buffer, len+2+2) == true);
}
